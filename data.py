import numpy as np
import pandas
import os
from math import sin, cos
from sklearn.utils import shuffle

MAX_SAMPLES = 50

def correct_accel_orientation(df):
    for i in range(len(df)):
        pitch, roll, yaw, ax, ay, az = df['pitch'][i], df['roll'][i], df['yaw'][i], df['ax'][i], df['ay'][i], df['az'][i]
        # generate rotation matrix
        matrix = np.matrix([[cos(yaw)*cos(roll), cos(yaw)*sin(roll)*sin(pitch)-sin(yaw)*cos(pitch), cos(yaw)*sin(roll)*cos(pitch)+sin(pitch)*sin(yaw)],
            [sin(yaw)*cos(roll), sin(pitch)*sin(roll)*sin(yaw)+cos(pitch)*cos(yaw), sin(yaw)*sin(roll)*cos(pitch)-cos(yaw)*sin(pitch)],
            [-sin(roll), cos(roll)*sin(pitch), cos(roll)*cos(pitch)]])
        df['ax'][i] = matrix[0,0]*ax + matrix[0,1]*ay + matrix[0,2]*az
        df['ay'][i] = matrix[1,0]*ax + matrix[1,1]*ay + matrix[1,2]*az
        df['az'][i] = matrix[2,0]*ax + matrix[2,1]*ay + matrix[2,2]*az

def get_csv_data(location):
    data = pandas.read_csv(location)
    df = pandas.DataFrame(data)
    correct_accel_orientation(df)
    df = df.drop(columns=['pitch', 'roll', 'yaw']) #TODO:

    return df.as_matrix()


def get_csvs_in_folder(folder):
    csvs = []
    for root, dirs, files in os.walk(folder):
        for name in files:
            print "found file", name
            csv = get_csv_data(os.path.join(root, name))
            csv = np.r_[csv, np.zeros((MAX_SAMPLES-csv.shape[0], 3))] # fill up to 50 timesteps
            csvs.append(csv)
    return np.array(csvs)


def load_data(use_coreml=False):
    X = []#np.array([], shape=(0,,3))
    y = np.array([])

    for i in range(0, 10):
        data = get_csvs_in_folder("training_data/"+str(i))
        #X = np.r_[X, data] #X.append(data) #X = np.append(X, [data], axis=0)
        for dd in data:
            X.append(dd)
        y = np.r_[y, [i] * len(data)]

    X = np.array(X)

    X, y = shuffle(X, y)

    if use_coreml:
        X = X.reshape(X.shape[0], X.shape[1]*X.shape[2])

    #row = int(round(0.9 * X.shape[0]))
    #X_train = X[:row]
    #y_train = y[:row]
    #X_test = X[row:]
    #y_test = y[row:]

    return X, y#X_train, y_train, X_test, y_test
