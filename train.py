import numpy as np
import math
import os
import coremltools
import h5py
from data import * #custom
from model import * #custom
import matplotlib.pyplot as plt

USE_CORE_ML = True


X_train, y_train = load_data(use_coreml=USE_CORE_ML) #.., X_test, y_test

print("x_train", X_train.shape)
print("y_train", y_train.shape)
#print("x_test", X_test.shape)
#print("y_test", y_test.shape)

model = build_model(use_coreml=USE_CORE_ML)
model.summary()


#train model
training = model.fit(X_train, y_train, batch_size=10, epochs=200, validation_split=0.1, verbose=1)

#evaluate trained model
trainScore = model.evaluate(X_train, y_train, verbose=0)
print "train score: %.2f MSE (%.2f RMSE)" % (trainScore[0], math.sqrt(trainScore[0]))

#testScore = model.evaluate(X_test, y_test, verbose=0)
#print "test score: %.2f MSE (%.2f RMSE)" % (testScore[0], math.sqrt(testScore[0]))


# save Keras model
model.save("models/model.h5")


# Core ML Convertion
coreml_model = coremltools.converters.keras.convert(model)
coreml_model.author = "Alexander Eichhorn"
coreml_model.short_description = "Number Recognition"
coreml_model.save("models/NumberRecognition.mlmodel")



# plot accuracy over time
plt.figure(1)
a = plt.subplot(211)
plt.title('Model accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.grid()
plt.plot(training.history['acc'], label='acc')
plt.plot(training.history['val_acc'], label='val_acc')
plt.legend()

b = plt.subplot(212)
plt.title('Model loss')
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.grid()
plt.plot(training.history['loss'], label='loss')
plt.plot(training.history['val_loss'], label='val_loss')
plt.legend()

plt.show()
