import numpy as np
import pandas
import math
from data import * #custom
from model import * #custom
from sklearn.model_selection import GridSearchCV
from keras.wrappers.scikit_learn import KerasClassifier


X_train, y_train = load_data()


model = KerasClassifier(build_fn=build_model, verbose=1) # TODO: add epochs, batch_size when known

# grid search parameters
batch_size = [10, 15, 20] #best: 15
epochs = [150, 200, 250] # best: 50 (++)
hidden_neurons = [140] # best: 110 (+- 15)
optimizer = ['sgd', 'rmsprop', 'adagrad', 'adadelta', 'adam', 'adamax', 'nadam'] #best: adagrad

grid_params = dict(batch_size=batch_size, epochs=epochs, hidden_neurons=hidden_neurons)

grid = GridSearchCV(estimator=model, param_grid=grid_params, n_jobs=-1) # n_jobs=-1: use all cores available
grid_result = grid.fit(X_train, y_train)


# summarize results
print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))
