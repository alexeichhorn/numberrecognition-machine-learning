from keras.models import Sequential
from keras.layers import Dense, Activation, Reshape
from keras.layers import LSTM


MAX_SAMPLES = 50 # 5s * 10 Hz
CLASSIFICATION_TYPES = 10 # 0-9

def build_model(hidden_neurons=140, optimizer='adamax', use_coreml=False):
    model = Sequential()
    if use_coreml: model.add(Reshape(input_shape=(MAX_SAMPLES*3,), target_shape=(MAX_SAMPLES, 3))) #coreml can only handle 1D arrays
    model.add(LSTM(hidden_neurons, input_shape=(MAX_SAMPLES, 3)))
    model.add(Dense(CLASSIFICATION_TYPES))
    model.add(Activation("softmax"))

    model.compile(loss='sparse_categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    return model
